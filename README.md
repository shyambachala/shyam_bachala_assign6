##GENERIC CHECKPOINTING | JAVA | DESIGN PATTERNS

##Project description
Code Language 			: Java

Patterns Implemented	: Dynamic Proxy and Strategy Pattern

•	Reflection feature of Java is used here.

•	Serializing java objects to XML and de-serializing XML data to java objects using reflection. Here the Dynamic Proxy Pattern and Strategy Pattern are used.


## To clean:
ant -buildfile src/build.xml clean

-----------------------------------------------------------------------
## To compile:

ant -buildfile src/build.xml all

Description: Compiles your code and generates .class files inside the BUILD
folder.

-----------------------------------------------------------------------

## To run by code:

ant -buildfile src/build.xml run -Darg0=mode -Darg1=N -Darg2=fileName

Description:

Testing the input.txt file has been done by placing it in the [kumaralakshmanmahipal_damera_shyam_bachala_assign_5/fileVisitors]
as mentioned in the design requirements.

-------------------------------------------------------------------------
## To create tarball for submission

ant -buildfile src/build.xml tarzip

-------------------------------------------------------------------------
