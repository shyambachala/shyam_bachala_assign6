package genericCheckpointing.xmlStoreRestore;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import genericCheckpointing.util.FileProcessor;
import genericCheckpointing.util.SerializableObject;

/**
 * The Class DeserializeTypes.
 */
public class DeserializeTypes 
{
	/** The freader. */
	FileProcessor freader = null;

	/**
	 * Instantiates a new deserialize types.
	 * @param freader the freader
	 */
	public DeserializeTypes(FileProcessor freader) 
	{
		this.freader = freader;
	}
	
	/**
	 * Deserialize xml.
	 * @return the object
	 */
	public Object deserializeXml() throws NoSuchMethodException, SecurityException, NumberFormatException, IllegalAccessException, IllegalArgumentException, InvocationTargetException
	{
		String inputLine = "";
		boolean readFlag = false;
		Class<?> clazz = null;
		SerializableObject clazzObj = null;
		Method clazzMethod = null;
		
		while((inputLine = freader.readLine()) != null)
	    {
	      	if(!(inputLine.trim().length() > 0))
	      	{
	      		continue;
	      	}
	      	if(inputLine.equals("<DPSerialization>"))
	      	{
	      		readFlag = true;
	      		continue;
	      	}
	      	if(inputLine.equals("</DPSerialization>"))
	      	{
	      		break;
	      	}
	      	
	      	if(readFlag && (!inputLine.contains("</complexType>")))
	      	{
	      		if(inputLine.contains("complexType"))
	      		{
	      			try 
	      			{
						clazz = Class.forName(getClassName(inputLine));
						clazzObj = (SerializableObject) clazz.newInstance();
					}
	      			catch (ClassNotFoundException e) 
	      			{
	      				System.out.println(e.getMessage());
						e.printStackTrace();
					} 
	      			catch (InstantiationException e) 
	      			{
	      				System.out.println(e.getMessage());
						e.printStackTrace();
					} 
	      			catch (IllegalAccessException e) 
	      			{
	      				System.out.println(e.getMessage());
						e.printStackTrace();
					}
	      			finally
	      			{}
	      		}
	      		else if(inputLine.contains("myInt"))
	    		{
	    			clazzMethod = clazz.getDeclaredMethod("setMyInt", int.class);
	    			clazzMethod.invoke(clazzObj, Integer.parseInt(getDataMemValue(inputLine)));
	    		}
	    		else if(inputLine.contains("myOtherInt"))
	    		{
	    			clazzMethod = clazz.getDeclaredMethod("setMyOtherInt", int.class);
	    			clazzMethod.invoke(clazzObj, Integer.parseInt(getDataMemValue(inputLine)));
	    		}
	    		else if(inputLine.contains("myLong"))
	    		{
	    			clazzMethod = clazz.getDeclaredMethod("setMyLong", long.class);
	    			clazzMethod.invoke(clazzObj, Long.parseLong(getDataMemValue(inputLine)));
	    		}
	    		else if(inputLine.contains("myOtherLong"))
	    		{
	    			clazzMethod = clazz.getDeclaredMethod("setMyOtherLong", long.class);
	    			clazzMethod.invoke(clazzObj, Long.parseLong(getDataMemValue(inputLine)));
	    		}
	    		else if(inputLine.contains("myString"))
	    		{
	    			clazzMethod = clazz.getDeclaredMethod("setMyString", String.class);
	    			clazzMethod.invoke(clazzObj, getDataMemValue(inputLine));
	    		}
	    		else if(inputLine.contains("myBool"))
	    		{
	    			clazzMethod = clazz.getDeclaredMethod("setMyBool", boolean.class);
	    			clazzMethod.invoke(clazzObj, Boolean.parseBoolean(getDataMemValue(inputLine)));
	    		}
	    		else if(inputLine.contains("myDoubleT"))
	    		{
	    			clazzMethod = clazz.getDeclaredMethod("setMyDoubleT", double.class);
	    			clazzMethod.invoke(clazzObj, Double.parseDouble(getDataMemValue(inputLine)));
	    		}
	    		else if(inputLine.contains("myOtherDoubleT"))
	    		{
	    			clazzMethod = clazz.getDeclaredMethod("setMyOtherDoubleT", double.class);
	    			clazzMethod.invoke(clazzObj, Double.parseDouble(getDataMemValue(inputLine)));
	    		}
	    		else if(inputLine.contains("myFloatT"))
	    		{
	    			clazzMethod = clazz.getDeclaredMethod("setMyFloatT", float.class);
	    			clazzMethod.invoke(clazzObj, Float.parseFloat(getDataMemValue(inputLine)));
	    		}
	    		else if(inputLine.contains("myShortT"))
	    		{
	    			clazzMethod = clazz.getDeclaredMethod("setMyShortT", short.class);
	    			clazzMethod.invoke(clazzObj, Short.parseShort(getDataMemValue(inputLine)));
	    		}
	    		else if(inputLine.contains("myOtherShortT"))
	    		{
	    			clazzMethod = clazz.getDeclaredMethod("setMyOtherShortT", short.class);
	    			clazzMethod.invoke(clazzObj, Short.parseShort(getDataMemValue(inputLine)));
	    		}
	    		else if(inputLine.contains("myCharT"))
	    		{
	    			clazzMethod = clazz.getDeclaredMethod("setMyCharT", char.class);
	    			clazzMethod.invoke(clazzObj, getDataMemValue(inputLine).charAt(0));
	    		}
	    		else
	    		{
	    			System.err.println("Error at deserialize");
	    		}
	      	}  	
	    }
		return clazzObj;
	}
	
	/**
	 * Gets the class name.
	 * @param inline the inline
	 * @return the class name
	 */
	public String getClassName(String inline)
	{
		String str = inline;
       	String[] node_inputs = str.split("\"");
		return node_inputs[1];
	}
	
	/**
	 * Gets the data mem value.
	 * @param inline the inline
	 * @return the data mem value
	 */
	public String getDataMemValue(String inline)
	{
		String[] in = inline.split(">");
	    String[] in1 = in[1].split("<");
		return in1[0];
	}
}