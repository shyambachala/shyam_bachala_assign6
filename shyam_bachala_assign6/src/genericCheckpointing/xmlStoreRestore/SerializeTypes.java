package genericCheckpointing.xmlStoreRestore;

/**
 * The Class SerializeTypes.
 */
public class SerializeTypes 
{
	/**
	 * Gets the pseudo xml code.
	 * @param dataMember the data member
	 * @param dataValue the data value
	 * @return the pseudo xml code
	 */
	public String getPseudoXmlCode(String dataMember, Object dataValue)
	{
		String serializedStr = "";
	
		if(dataMember.equals("myInt"))
		{
			serializedStr = serializeMyInt("myInt", (int)dataValue);
		}
		else if(dataMember.equals("myOtherInt"))
		{
			serializedStr = serializeMyOtherInt("myOtherInt", (int)dataValue);
		}
		else if(dataMember.equals("myLong"))
		{
			serializedStr = serializeMyLong("myLong", (long)dataValue);
		}
		else if(dataMember.equals("myOtherLong"))
		{
			serializedStr = serializeMyOtherLong("myOtherLong", (long)dataValue);
		}
		else if(dataMember.equals("myString"))
		{
			serializedStr = serializeMyString("myString", (String)dataValue);
		}
		else if(dataMember.equals("myBool"))
		{
			serializedStr = serializeMyBool("myBool", (boolean)dataValue);
		}
		else if(dataMember.equals("myDoubleT"))
		{
			serializedStr = serializeMyDoubleT("myDoubleT", (double)dataValue);
		}
		else if(dataMember.equals("myOtherDoubleT"))
		{
			serializedStr = serializeMyOtherDoubleT("myOtherDoubleT", (double)dataValue);
		}
		else if(dataMember.equals("myFloatT"))
		{
			serializedStr = serializeMyFloatT("myFloatT", (float)dataValue);
		}
		else if(dataMember.equals("myShortT"))
		{
			serializedStr = serializeMyShortT("myShortT", (short)dataValue);
		}
		else if(dataMember.equals("myOtherShortT"))
		{
			serializedStr = serializeMyOtherShortT("myOtherShortT", (short)dataValue);
		}
		else if(dataMember.equals("myCharT"))
		{
			serializedStr = serializeMyCharT("myCharT", (char)dataValue);
		}
		else
		{
			serializedStr = "NOT FOUND";
		}
		
		return serializedStr;
	}

	/**
	 * Serialize my int.
	 * @param strPart the str part
	 * @param dataValue the data value
	 * @return the string
	 */
	private String serializeMyInt(String strPart, int dataValue) 
	{
		return "  <" + strPart + " xsi:type=\"xsd:int\">" + dataValue + "</" + strPart + ">\n";
	}
	
	/**
	 * Serialize my other int.
	 * @param strPart the str part
	 * @param dataValue the data value
	 * @return the string
	 */
	private String serializeMyOtherInt(String strPart, int dataValue) 
	{
		return "  <" + strPart + " xsi:type=\"xsd:int\">" + dataValue + "</" + strPart + ">\n";
	}
	
	/**
	 * Serialize my long.
	 * @param strPart the str part
	 * @param dataValue the data value
	 * @return the string
	 */
	private String serializeMyLong(String strPart, long dataValue) 
	{
		return "  <" + strPart + " xsi:type=\"xsd:long\">" + dataValue + "</" + strPart + ">\n";
	}
	
	/**
	 * Serialize my other long.
	 * @param strPart the str part
	 * @param dataValue the data value
	 * @return the string
	 */
	private String serializeMyOtherLong(String strPart, long dataValue) 
	{
		return "  <" + strPart + " xsi:type=\"xsd:long\">" + dataValue + "</" + strPart + ">\n";
	}
	
	/**
	 * Serialize my string.
	 * @param strPart the str part
	 * @param dataValue the data value
	 * @return the string
	 */
	private String serializeMyString(String strPart, String dataValue) 
	{
		return "  <" + strPart + " xsi:type=\"xsd:String\">" + dataValue + "</" + strPart + ">\n";
	}
	
	/**
	 * Serialize my bool.
	 * @param strPart the str part
	 * @param dataValue the data value
	 * @return the string
	 */
	private String serializeMyBool(String strPart, boolean dataValue) 
	{
		return "  <" + strPart + " xsi:type=\"xsd:boolean\">" + dataValue + "</" + strPart + ">\n";
	}
	
	/**
	 * Serialize my double T.
	 * @param strPart the str part
	 * @param dataValue the data value
	 * @return the string
	 */
	private String serializeMyDoubleT(String strPart, double dataValue) 
	{
		return "  <" + strPart + " xsi:type=\"xsd:double\">" + dataValue + "</" + strPart + ">\n";
	}
	
	/**
	 * Serialize my other double T.
	 * @param strPart the str part
	 * @param dataValue the data value
	 * @return the string
	 */
	private String serializeMyOtherDoubleT(String strPart, double dataValue) 
	{
		return "  <" + strPart + " xsi:type=\"xsd:double\">" + dataValue + "</" + strPart + ">\n";
	}
	
	/**
	 * Serialize my float T.
	 * @param strPart the str part
	 * @param dataValue the data value
	 * @return the string
	 */
	private String serializeMyFloatT(String strPart, float dataValue) 
	{
		return "  <" + strPart + " xsi:type=\"xsd:float\">" + dataValue + "</" + strPart + ">\n";
	}
	
	/**
	 * Serialize my short T.
	 * @param strPart the str part
	 * @param dataValue the data value
	 * @return the string
	 */
	private String serializeMyShortT(String strPart, short dataValue) 
	{
		return "  <" + strPart + " xsi:type=\"xsd:short\">" + dataValue + "</" + strPart + ">\n";
	}
	
	/**
	 * Serialize my other short T.
	 * @param strPart the str part
	 * @param dataValue the data value
	 * @return the string
	 */
	private String serializeMyOtherShortT(String strPart, short dataValue) 
	{
		return "  <" + strPart + " xsi:type=\"xsd:short\">" + dataValue + "</" + strPart + ">\n";
	}
	
	/**
	 * Serialize my char T.
	 * @param strPart the str part
	 * @param dataValue the data value
	 * @return the string
	 */
	private String serializeMyCharT(String strPart, char dataValue) 
	{
		return "  <" + strPart + " xsi:type=\"xsd:char\">" + dataValue + "</" + strPart + ">\n";
	}
}