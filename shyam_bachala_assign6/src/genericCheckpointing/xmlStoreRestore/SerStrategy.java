package genericCheckpointing.xmlStoreRestore;

import genericCheckpointing.util.SerializableObject;

/**
 * The Interface SerStrategy.
 */
public interface SerStrategy 
{
	/**
	 * Process input.
	 * @param sObject the s object
	 */
	void processInput(SerializableObject sObject);
}
