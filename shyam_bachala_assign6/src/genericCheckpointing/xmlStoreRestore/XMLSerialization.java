package genericCheckpointing.xmlStoreRestore;

import java.lang.reflect.Field;
import genericCheckpointing.util.FileProcessor;
import genericCheckpointing.util.SerializableObject;

/**
 * The Class XMLSerialization.
 */
public class XMLSerialization implements SerStrategy 
{
	
	/** The fwriter. */
	FileProcessor fwriter = null;
	
	/** The concat string. */
	String concat_string;
	
	/** The generate pseudo xml. */
	SerializeTypes generatePseudoXml = null;

	/**
	 * Instantiates a new XML serialization.
	 * @param fw the fw
	 */
	public XMLSerialization(FileProcessor fw) 
	{
		this.fwriter = fw;
		this.concat_string = "";
		this.generatePseudoXml = new SerializeTypes();
	}

	@Override
	public void processInput(SerializableObject sObject) 
	{
		Class<? extends SerializableObject> myClass = sObject.getClass();
		String aClass = myClass.getName();
		concat_string += "<DPSerialization>\n";
		concat_string += " <complexType xsi:type=\"" + aClass + "\">\n";
		Field[] fields = myClass.getDeclaredFields();
		for (Field f : fields) 
		{
			Object dataValue = null;
			String fieldName = f.getName();
			f.setAccessible(true);
			try 
			{
				dataValue = f.get(sObject);
			} catch (IllegalArgumentException e) {
				System.out.println("Illegal Argument Exception");
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				System.out.println("Illegal Access Exception");
				e.printStackTrace();
			}
			concat_string += generatePseudoXml.getPseudoXmlCode(fieldName, dataValue);
		}
		concat_string += " </complexType>\n</DPSerialization>\n";
		fwriter.writeOutput(concat_string);
	}
}
