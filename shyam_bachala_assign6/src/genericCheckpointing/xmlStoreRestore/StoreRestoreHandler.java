package genericCheckpointing.xmlStoreRestore;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

import genericCheckpointing.util.FileProcessor;
import genericCheckpointing.util.SerializableObject;

/**
 * The Class StoreRestoreHandler.
 */
public class StoreRestoreHandler implements InvocationHandler 
{
	
	/** The file name. */
	private String fileName;
	
	/** The freader. */
	private FileProcessor freader = null;
	
	/** The fwriter. */
	private FileProcessor fwriter = null;

	@Override
	public Object invoke(Object proxy, Method m, Object[] args) throws Throwable 
	{	
		String m_called = m.getName();

//		Serialization when method called is writeObj
		if (m_called.equals("writeObj") && args[1].equals("XML")) 
		{
			SerStrategy xmlSer = new XMLSerialization(getFwriter());
			serializeData((SerializableObject) args[0], xmlSer);
		}

//		De-serialization when method called is readObj
		if (m_called.equals("readObj") && (args[0].equals("XML"))) 
		{
			DeserializeTypes xmlDeser = new DeserializeTypes(getFreader());
			Object obj = xmlDeser.deserializeXml();
			return obj;
		}
		return null;
	}

	/**
	 * Serialize data.
	 * @param sObject the s object
	 * @param sStrategy the s strategy
	 */
	public void serializeData(SerializableObject sObject, SerStrategy sStrategy) {
		sStrategy.processInput(sObject);
	}

	/**
	 * Close file processor read.
	 */
	public void closeFileProcessorRead() {
		freader.remove_read();
	}

	/**
	 * Close file processor write.
	 */
	public void closeFileProcessorWrite() {
		fwriter.remove_write();
	}

	/**
	 * Gets the file name.
	 * @return the file name
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * Sets the file name.
	 * @param fileName the new file name
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * Gets the freader.
	 * @return the freader
	 */
	public FileProcessor getFreader() {
		return freader;
	}

	/**
	 * Sets the freader.
	 * @param freader the new freader
	 */
	public void setFreader(FileProcessor freader) {
		this.freader = freader;
	}

	/**
	 * Gets the fwriter.
	 * @return the fwriter
	 */
	public FileProcessor getFwriter() {
		return fwriter;
	}

	/**
	 * Sets the fwriter.
	 * @param fwriter the new fwriter
	 */
	public void setFwriter(FileProcessor fwriter) {
		this.fwriter = fwriter;
	}
}