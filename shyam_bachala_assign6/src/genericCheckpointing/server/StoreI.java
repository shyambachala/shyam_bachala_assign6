package genericCheckpointing.server;

import genericCheckpointing.util.MyAllTypesFirst;
import genericCheckpointing.util.MyAllTypesSecond;

/**
 * The Interface StoreI.
 */
public interface StoreI extends StoreRestoreI
{
	/**
	 * Write obj.
	 * @param myFirst the my first
	 * @param string the string
	 */
	void writeObj(MyAllTypesFirst myFirst, String string);
	
	/**
	 * Write obj.
	 * @param mySecond the my second
	 * @param string the string
	 */
	void writeObj(MyAllTypesSecond mySecond, String string);
}
