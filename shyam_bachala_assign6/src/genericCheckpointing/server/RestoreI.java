package genericCheckpointing.server;

import genericCheckpointing.util.SerializableObject;

/**
 * The Interface RestoreI.
 */
public interface RestoreI extends StoreRestoreI
{
	
	/**
	 * Read obj.
	 * @param string the string
	 * @return the serializable object
	 */
	SerializableObject readObj(String string);
}
