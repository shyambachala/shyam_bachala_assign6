package genericCheckpointing.util;

/**
 * The Class MyAllTypesFirst.
 */
public class MyAllTypesFirst extends SerializableObject {
	/** The my int. */
	int myInt = 0;

	/** The my other int. */
	int myOtherInt = 0;

	/** The my long. */
	long myLong = 0;

	/** The my other long. */
	long myOtherLong = 0;

	/** The my string. */
	String myString = "";

	/** The my bool. */
	boolean myBool = false;

	/**
	 * Instantiates a new my all types first.
	 */
	public MyAllTypesFirst() {
	}

	/**
	 * Instantiates a new my all types first.
	 *
	 * @param myInt the my int
	 * @param myOtherInt the my other int
	 * @param myLong the my long
	 * @param myOtherLong the my other long
	 * @param myString the my string
	 * @param myBool the my bool
	 */
	public MyAllTypesFirst(int myInt, int myOtherInt, long myLong, long myOtherLong, String myString, boolean myBool) {
		this.myInt = myInt;
		this.myOtherInt = myOtherInt;
		this.myLong = myLong;
		this.myOtherLong = myOtherLong;
		this.myString = myString;
		this.myBool = myBool;
	}

	/**
	 * Gets the my int.
	 * @return the my int
	 */
	public int getMyInt() {
		return myInt;
	}

	/**
	 * Sets the my int.
	 * @param myInt the new my int
	 */
	public void setMyInt(int myInt) {
		this.myInt = myInt;
	}

	/**
	 * Gets the my other int.
	 * @return the my other int
	 */
	public int getMyOtherInt() {
		return myOtherInt;
	}

	/**
	 * Sets the my other int.
	 * 
	 * @param myOtherInt the new my other int
	 */
	public void setMyOtherInt(int myOtherInt) {
		this.myOtherInt = myOtherInt;
	}

	/**
	 * Gets the my long.
	 * @return the my long
	 */
	public long getMyLong() {
		return myLong;
	}

	/**
	 * Sets the my long.
	 * @param myLong the new my long
	 */
	public void setMyLong(long myLong) {
		this.myLong = myLong;
	}

	/**
	 * Gets the my other long.
	 * @return the my other long
	 */
	public long getMyOtherLong() {
		return myOtherLong;
	}

	/**
	 * Sets the my other long.
	 * @param myOtherLong the new my other long
	 */
	public void setMyOtherLong(long myOtherLong) {
		this.myOtherLong = myOtherLong;
	}

	/**
	 * Gets the my string.
	 * @return the my string
	 */
	public String getMyString() {
		return myString;
	}

	/**
	 * Sets the my string.
	 * @param myString the new my string
	 */
	public void setMyString(String myString) {
		this.myString = myString;
	}

	/**
	 * Checks if is my bool.
	 * @return true, if is my bool
	 */
	public boolean isMyBool() {
		return myBool;
	}

	/**
	 * Sets the my bool.
	 * @param myBool the new my bool
	 */
	public void setMyBool(boolean myBool) {
		this.myBool = myBool;
	}

	@Override
	public boolean equals(Object checkObj) {
		if ((checkObj == null) || (getClass() != checkObj.getClass())){
			return false;
		}

		MyAllTypesFirst deserObj = (MyAllTypesFirst) checkObj;
		if (myInt != deserObj.myInt) {
			return false;
		} else if (myOtherInt != deserObj.myOtherInt) {
			return false;
		} else if (myLong != deserObj.myLong) {
			return false;
		} else if (myOtherLong != deserObj.myOtherLong) {
			return false;
		} else if (!myString.equals(deserObj.myString)) {
			return false;
		} else if (myBool != deserObj.myBool) {
			return false;
		} else {
			return true;
		}
	}
	
	public String toString() {
		String str ="";
		str += "myInt: " + Integer.toString(myInt) + "\n";
		str += "myOtherInt: " + Integer.toString(myOtherInt) + "\n";
		str += "myLong: " + Long.toString(myLong) + "\n";
		str += "myOtherLong: " + Long.toString(myOtherLong) + "\n";
		str += "myString: " + myString + "\n";
		str += "myBool: " + Boolean.toString(myBool) + "\n";
		return str;
	}
}
