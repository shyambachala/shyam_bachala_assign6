package genericCheckpointing.util;

/**
 * The Class MyAllTypesSecond.
 */
public class MyAllTypesSecond extends SerializableObject {
	/** The my double T. */
	double myDoubleT = 0.0;

	/** The my other double T. */
	double myOtherDoubleT = 0.0;

	/** The my float T. */
	float myFloatT = (float) 0.0;

	/** The my short T. */
	short myShortT = 0;

	/** The my other short T. */
	short myOtherShortT = 0;

	/** The my char T. */
	char myCharT;

	/**
	 * Instantiates a new my all types second.
	 */
	public MyAllTypesSecond() {
	}

	/**
	 * Instantiates a new my all types second.
	 * @param myDoubleT the my double T
	 * @param myOtherDoubleT the my other double T
	 * @param myFloatT the my float T
	 * @param myShortT the my short T
	 * @param myOtherShortT the my other short T
	 * @param myCharT the my char T
	 */
	public MyAllTypesSecond(double myDoubleT, double myOtherDoubleT, float myFloatT, short myShortT,
			short myOtherShortT, char myCharT) {
		this.myDoubleT = myDoubleT;
		this.myOtherDoubleT = myOtherDoubleT;
		this.myFloatT = myFloatT;
		this.myShortT = myShortT;
		this.myOtherShortT = myOtherShortT;
		this.myCharT = myCharT;
	}

	/**
	 * Gets the my double T.
	 * @return the my double T
	 */
	public double getMyDoubleT() {
		return myDoubleT;
	}

	/**
	 * Sets the my double T.
	 * @param myDoubleT the new my double T
	 */
	public void setMyDoubleT(double myDoubleT) {
		this.myDoubleT = myDoubleT;
	}

	/**
	 * Gets the my other double.
	 * @return the my other double
	 */
	public double getMyOtherDouble() {
		return myOtherDoubleT;
	}

	/**
	 * Sets the my other double T.
	 * @param myOtherDouble the new my other double T
	 */
	public void setMyOtherDoubleT(double myOtherDouble) {
		this.myOtherDoubleT = myOtherDouble;
	}

	/**
	 * Gets the my float T.
	 * @return the my float T
	 */
	public float getMyFloatT() {
		return myFloatT;
	}

	/**
	 * Sets the my float T.
	 * @param myFloatT the new my float T
	 */
	public void setMyFloatT(float myFloatT) {
		this.myFloatT = myFloatT;
	}

	/**
	 * Gets the my short T.
	 * @return the my short T
	 */
	public short getMyShortT() {
		return myShortT;
	}

	/**
	 * Sets the my short T.
	 * @param myShortT the new my short T
	 */
	public void setMyShortT(short myShortT) {
		this.myShortT = myShortT;
	}

	/**
	 * Gets the my other short T.
	 * @return the my other short T
	 */
	public short getMyOtherShortT() {
		return myOtherShortT;
	}

	/**
	 * Sets the my other short T.
	 * @param myOtherShortT the new my other short T
	 */
	public void setMyOtherShortT(short myOtherShortT) {
		this.myOtherShortT = myOtherShortT;
	}

	/**
	 * Gets the my char T.
	 * @return the my char T
	 */
	public char getMyCharT() {
		return myCharT;
	}

	/**
	 * Sets the my char T.
	 * @param myCharT the new my char T
	 */
	public void setMyCharT(char myCharT) {
		this.myCharT = myCharT;
	}

	@Override
	public boolean equals(Object checkObj) {
		if ((checkObj == null) || (getClass() != checkObj.getClass())) {
			return false;
		}
		MyAllTypesSecond deserObj = (MyAllTypesSecond) checkObj;
		if (Double.compare(myDoubleT, deserObj.myDoubleT) != 0) {
			return false;
		} else if (Double.compare(myOtherDoubleT, deserObj.myOtherDoubleT) != 0) {
			return false;
		} else if (Float.compare(myFloatT, deserObj.myFloatT) != 0) {
			return false;
		} else if (myShortT != deserObj.myShortT) {
			return false;
		} else if (myOtherShortT != deserObj.myOtherShortT) {
			return false;
		} else if (myCharT != deserObj.myCharT) {
			return false;
		} else {
			return true;
		}
	}
	
	public String toString() {
		String str ="";
		str += "myDoubleT: " + Double.toString(myDoubleT) + "\n";
		str += "myOtherDoubleT: " + Double.toString(myOtherDoubleT) + "\n";
		str += "myFloatT: " + Float.toString(myFloatT) + "\n";
		str += "myShortT: " + Short.toString(myShortT) + "\n";
		str += "myOtherShortT: " + Short.toString(myOtherShortT) + "\n";
		str += "myCharT: " + Character.toString(myCharT) + "\n";
		return str;
	}
}