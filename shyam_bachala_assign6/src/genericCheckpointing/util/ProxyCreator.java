package genericCheckpointing.util;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

import genericCheckpointing.server.StoreRestoreI;

/**
 * The Class ProxyCreator.
 */
public class ProxyCreator 
{
	/**
	 * Creates the proxy.
	 * @param interfaceArray the interface array
	 * @param handler the handler
	 * @return the store restore I
	 */
	public StoreRestoreI createProxy(Class<?>[] interfaceArray, InvocationHandler handler)
	{
		StoreRestoreI storeRestoreRef = (StoreRestoreI) Proxy.newProxyInstance(getClass().getClassLoader(), interfaceArray, handler);
		return storeRestoreRef;
	}
}
