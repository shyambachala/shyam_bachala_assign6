package genericCheckpointing.driver;

import java.io.File;
import java.util.Random;
import java.util.UUID;
import java.util.Vector;

import genericCheckpointing.server.RestoreI;
import genericCheckpointing.server.StoreI;
import genericCheckpointing.server.StoreRestoreI;
import genericCheckpointing.util.FileProcessor;
import genericCheckpointing.util.MyAllTypesFirst;
import genericCheckpointing.util.MyAllTypesSecond;
import genericCheckpointing.util.ProxyCreator;
import genericCheckpointing.util.SerializableObject;
import genericCheckpointing.xmlStoreRestore.StoreRestoreHandler;

/**
 * The Class Driver.
 */
public class Driver 
{	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) 
	{		
//		Check for the command line arguements
		if (args.length != 3) 
		{
			System.out.println("INVALID NUMBER OF INPUT PARAMETERS: " + args.length + "\nGIVE INPUT AS BELOW\n"
								+ "ant -buildfile src/build.xml run -Darg0=MODE -Darg1=N -Darg2=FILE_NAME");
			System.exit(3);
		}
//		Variables for setting mode of operation and filename. 
		String mode = args[0];
		String filename = args[2];
//		NUM_OF_OBJECTS refers to the count for each of MyAllTypesFirst and MyAllTypesSecond
		int NUM_OF_OBJECTS = Integer.parseInt(args[1]);
		
		ProxyCreator pc = new ProxyCreator();

		// instance of StoreRestoreHandler
		StoreRestoreHandler srHand = new StoreRestoreHandler();

		// Instance of Proxy
		StoreRestoreI cpointRef = (StoreRestoreI) pc.createProxy(new Class[] { StoreI.class, RestoreI.class }, srHand);

		MyAllTypesFirst myFirst;
		MyAllTypesSecond mySecond;

//		File Reader and File Writer
		FileProcessor fw = null;
		FileProcessor fr = null;

//		Set File Writer and Reader appropriate for the mode
		if (mode.equals("deser"))
		{
			fr = new FileProcessor(new File(filename), true);
			srHand.setFreader(fr);

		}
		
		if (mode.equals("serdeser")) 
		{
			fw = new FileProcessor(new File(filename), false);
			fr = new FileProcessor(new File(filename), true);

			srHand.setFileName(filename);
			srHand.setFwriter(fw);
			srHand.setFreader(fr);
		}

//		Random generator for generating the data for the objects
		Random rGen = new Random();
//		Vector for holding the Serializable Objects before serializing
		Vector<SerializableObject> serObjs = new Vector<SerializableObject>();

		if (mode.equals("serdeser")) 
		{
			for (int i = 0; i < NUM_OF_OBJECTS; i++) 
			{
				myFirst = new MyAllTypesFirst(intGen(rGen), intGen(rGen), longGen(rGen),
											  longGen(rGen), strGen(rGen), rGen.nextBoolean());
				mySecond = new MyAllTypesSecond(doubleGen(rGen), doubleGen(rGen), floatGen(rGen),
												shortGen(rGen), shortGen(rGen), charGen(rGen));
				serObjs.addElement(myFirst);
				serObjs.addElement(mySecond);

				((StoreI) cpointRef).writeObj(myFirst, "XML");
				((StoreI) cpointRef).writeObj(mySecond, "XML");
			}
			srHand.closeFileProcessorWrite();
		}

		SerializableObject myRecordRet;
//		Vector to store the returned objects
		Vector<SerializableObject> deserObjs = new Vector<SerializableObject>();

		for (int j = 0; j < 2 * NUM_OF_OBJECTS; j++) 
		{
			myRecordRet = ((RestoreI) cpointRef).readObj("XML");
//			store myRecordRet in the vector
			try 
			{
				deserObjs.addElement(myRecordRet);
			}
			catch (Exception e) 
			{
				System.out.println("Object not found");
				e.printStackTrace();
			}
			finally {}
		}
		srHand.closeFileProcessorRead();

		if(mode.equals("deser"))
		{
			for(int i = 0; i< 2*NUM_OF_OBJECTS; i++)
			{
				System.out.println("Instance "+ i);
				System.out.println(deserObjs.get(i).toString());
			}
		}
		
		int counter = 0;
		if(mode.equals("serdeser"))
		{
			if(serObjs.size()==deserObjs.size())
			for(int i = 0; i< 2*NUM_OF_OBJECTS; i++)
			{
				if(!serObjs.get(i).equals(deserObjs.get(i)))
					counter++;
			}
			System.out.println("NUMBER OF MISMATCHED INSTANCES = "+ counter);
		}
		
		
	}

	/**
	 * Str gen.
	 * @return the string
	 */
	public static String strGen(Random r) {
		String result = "";
		String str = "abcdefghijklmnopqrstuvwxyz";
	    for (int i = 0; i < 5; i++) {
	        int index = r.nextInt(str.length());
	        result += str.charAt(index);
	    }
	    return result;
	}
	
	/**
	 * Char gen.
	 * @param r the r
	 * @return the char
	 */
	public static char charGen(Random r) {
		return (char) (r.nextInt(26) + 'a');
	}
	
	/**
	 * Int gen.
	 * @param r the r
	 * @return the int
	 */
	public static int intGen(Random r) {
		return (int)r.nextInt(10000);
	}
	
	/**
	 * Long gen.
	 * @param r the r
	 * @return the long
	 */
	public static long longGen(Random r) {
		return (long)(1 + (long) (r.nextDouble() * (10000 - 1)));
	}
	
	/**
	 * Short gen.
	 * @param r the r
	 * @return the short
	 */
	public static short shortGen(Random r) {
		return (short) r.nextInt(Short.MAX_VALUE - 10000);
	}
	
	/**
	 * Float gen.
	 * @param r the r
	 * @return the float
	 */
	public static float floatGen(Random r) {
		return (float)(1 + r.nextFloat() * (10000));
	}
	
	/**
	 * Double gen.
	 * @param r the r
	 * @return the double
	 */
	public static double doubleGen(Random r) {
		return (double)(1 + r.nextDouble() * (10000));
	}
}
